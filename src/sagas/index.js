// src/sagas/index.js
import { all } from 'redux-saga/effects';
import { watchFetchOrderData } from './orderSagas';

export default function* rootSaga() {
  yield all([watchFetchOrderData()]);
}
