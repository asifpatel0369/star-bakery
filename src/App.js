
// src/App.js
import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import axios from 'axios';
import Dashboard from './components/DashBoard';
import TimeSelector from './components/TimeSelector';
import TimeSeriesCharts from './components/TimeSeriesCharts';
import BarChart from './components/BarChart';
import TopBranches from './components/TopBranches';
import Filter from './components/Filter';

const App = () => {
  const [orders, setOrders] = useState([]);
  const [types, setTypes] = useState([]);
  const [states, setStates] = useState([]);
  const [regions, setRegions] = useState([]);
  const [barChartData, setBarChartData] = useState(null);

  useEffect(() => {
    axios.get('/api/orders')
      .then(response => {
        setOrders(response.data);
        const uniqueTypes = [...new Set(response.data.map(order => order.item_type))];
        const uniqueStates = [...new Set(response.data.map(order => order.order_state))];
        const uniqueRegions = [...new Set(response.data.map(order => order.branch))];
        setTypes(uniqueTypes);
        setStates(uniqueStates);
        setRegions(uniqueRegions);

        // Process data for the bar chart
        const barChartData = {}; // Define a structure for the bar chart data
        response.data.forEach(order => {
          if (barChartData[order.item_type]) {
            barChartData[order.item_type]++;
          } else {
            barChartData[order.item_type] = 1;
          }
        });
        setBarChartData(barChartData);
      })
      .catch(error => console.error('Error fetching data:', error));
  }, []);

  return (
    <Router>
      <div className="App">
        <Route path="/">
          <Dashboard
            orders={orders}
            types={types}
            states={states}
            regions={regions}
            barChartData={barChartData} // Pass down barChartData
          />
        </Route>
        <Route path="/time-selector" element={TimeSelector} />
        <Route path="/time-series-charts" element={TimeSeriesCharts} />
        <Route path="/bar-chart" element={BarChart} />
        <Route path="/top-branches" element={TopBranches} />
        <Route path="/filter" element={Filter} />
      </div>
    </Router>
  );
};

// export default App;

ReactDOM.render(<App />, document.getElementById('root'));

// root.render(<RouterProvider router={AppRouter} />);

