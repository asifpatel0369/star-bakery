// src/components/BarChart.js
import React from 'react';
import { Bar } from 'react-chartjs-2';

const BarChart = ({ data }) => {
  return (
    <div className="border border-gray-300 p-4 mb-4">
      <Bar
        data={data} 
        options={{ maintainAspectRatio: false }}
      />
    </div>
  );
};

export default BarChart;
