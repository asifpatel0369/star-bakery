// src/components/TimeSeriesCharts.js
import React from 'react';
import { Line } from 'react-chartjs-2';

const TimeSeriesCharts = ({ data, selectedChart }) => {
  return (
    <div>
      {selectedChart === 'orders' && (
        <Line
          // data={data.orders} 
          options={{ maintainAspectRatio: false }}
          className="border border-gray-300 p-4 mb-4"
        />
      )}
      {selectedChart === 'value' && (
        <Line
          data={data.value}
          options={{ maintainAspectRatio: false }}
          className="border border-gray-300 p-4 mb-4"
        />
      )}
    </div>
  );
};

export default TimeSeriesCharts;
