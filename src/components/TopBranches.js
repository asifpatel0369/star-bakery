// src/components/TopBranches.js
import React from 'react';

const TopBranches = ({ branches }) => {
  return (
    <div className="border border-gray-300 p-4 mb-4">
      <h2 className="text-xl font-bold mb-2">Top 5 Branches</h2>
      <ul>
        {branches.map(branch => (
          <li key={branch.id}  className="mb-1">{branch.name}</li>
        ))}
      </ul>
    </div>
  );
};

export default TopBranches;
