// src/components/Dashboard.js
import React, { useState } from 'react';
import TimeSelector from './TimeSelector';
import TimeSeriesCharts from './TimeSeriesCharts';
import BarChart from './BarChart';
import TopBranches from './TopBranches';
import Filter from './Filter';

const Dashboard = ({ orders, types, states, regions }) => {
  const [selectedChart, setSelectedChart] = useState('orders');
  const [filteredOrders, setFilteredOrders] = useState(orders);

  // Sample data

  const lineChartData = {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
    datasets: [
      {
        label: 'Number of Orders',
        data: [30, 40, 35, 50, 45, 60],
        fill: false,
        borderColor: 'rgba(75,192,192,1)',
        borderWidth: 1,
      },
      {
        label: 'Total Value (in Rs)',
        data: [15000, 20000, 17500, 25000, 22500, 30000],
        fill: false,
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
      },
    ],
  };
  
  // Sample data for Bar Chart
  const barChartData = {
    labels: ['Cake', 'Cookies', 'Muffins'],
    datasets: [
      {
        label: 'Number of Orders',
        data: [50, 30, 20],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
        ],
        borderWidth: 1,
      },
    ],
  };
  
  // Sample data for Top Branches
  const topBranchesData = [
    { id: 1, name: 'Branch A' },
    { id: 2, name: 'Branch B' },
    { id: 3, name: 'Branch C' },
    { id: 4, name: 'Branch D' },
    { id: 5, name: 'Branch E' },
  ];

  const handleChartChange = (event) => {
    setSelectedChart(event.target.value);
  };

  const handleFilterChange = (event) => {
    // Implement filter logic here
    // You can dispatch actions to update Redux state
    const filterType = event.target.id; // type, state, or region
    const filterValue = event.target.value;

    setFilteredOrders(prevOrders => {
      return prevOrders.filter(order => {
        if (filterType === 'type' && filterValue !== '') {
          return order.item_type === filterValue;
        }
        if (filterType === 'state' && filterValue !== '') {
          return order.order_state === filterValue;
        }
        if (filterType === 'region' && filterValue !== '') {
          return order.branch.toString() === filterValue;
        }
        return true; // If no filter applied, return all orders
      });
    });    
  };

  return (
    <div>
      <h1>Star Bakery Analyst Dashboard</h1>
      <TimeSelector handleChange={handleChartChange} />
      <TimeSeriesCharts data={lineChartData} selectedChart={selectedChart} />
      <BarChart data={barChartData} />
      <TopBranches branches={topBranchesData} />
      <Filter
        types={types}
        states={states}
        regions={regions}
        handleFilterChange={handleFilterChange}
      />
    </div>
  );
};

export default Dashboard;
