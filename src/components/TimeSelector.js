// src/components/TimeSelector.js
import React from 'react';

const TimeSelector = ({ handleChange }) => {
  return (
    <div className='mb-4'>
      <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor="timeRange">Select Time Range:</label>
      <select id="timeRange" onChange={handleChange}  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
        <option value="hour">Hour</option>
        <option value="day">Day</option>
        <option value="week">Week</option>
        <option value="month">Month</option>
      </select>
    </div>
  );
};

export default TimeSelector;
