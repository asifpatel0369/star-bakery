// src/components/Filter.js
import React from 'react';

const Filter = ({ types, states, regions, handleFilterChange }) => {
  return (
    <div  className="border border-gray-300 p-4 mb-4">
      <h2 className="text-xl font-bold mb-2">Filter Data</h2>
      <div className="mb-2">
        <label htmlFor="type" className="block text-gray-700 text-sm font-bold mb-1">Type:</label>
        <select id="type" onChange={handleFilterChange}  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
          <option value="">All</option>
          {types.map(type => (
            <option key={type} value={type}>{type}</option>
          ))}
        </select>
      </div>
      <div className="mb-2">
        <label htmlFor="state"  className="block text-gray-700 text-sm font-bold mb-1">State:</label>
        <select id="state" onChange={handleFilterChange}  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
          <option value="">All</option>
          {/* {states.map(state => (
            <option key={state} value={state}>{state}</option>
          ))} */}
        </select>
      </div>
      <div>
        <label htmlFor="region"  className="block text-gray-700 text-sm font-bold mb-1">Region:</label>
        <select id="region" onChange={handleFilterChange} className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
          <option value="">All</option>
          {/* {regions.map(region => (
            <option key={region} value={region}>{region}</option>
          ))} */}
        </select>
      </div>
    </div>
  );
};

export default Filter;
