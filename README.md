# star-bakery



## Getting started
This is Star Bakery Dashboard. Here you can check the health of the business through a dashboard which consist of following 
widgets
• Time selector widget to select which time range he wanted to view the data
• Time Series charts of order data
o Zoom in/out feature on time series
o Rollups of orders by granularity of hour, day, week, month
o Show two charts
▪ number of order
▪ Total value of orders in Rupees
o Ability to select one or both the charts
• A bar chart widget showing orders divided each type (Cake, Muffin, Cookies)
• A bar chart widget showing order by state (Created, Shipped, Delivered, Canceled)
• A chart showing top 5 branches
• Allowing user to filter the output of order data time series chart by selecting type, state or 
region from other charts
• Optional : Widget for comparing data between any two hours/days/weeks on metrics like 
number and total value of orders
• Optional : Make dashboard customizable to drag and drop new widgets or change position 
of existing widget
To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Used Technologies

ReactJs, TailwindCss

## Installation 
- npm init
- npm install -D parcel
- npx parcel index.html
- npm install react
- npm install react-dom
- npx parcel build index.html
- update package.json file, edit scripts: start and build
- npm start
- npm install react-router-dom
- npm install -D tailwindcss postcss
- npx tailwindcss init
